'use strict';

var express = require('express');

var app = express();

app.set('port', (process.env.PORT || 5000));

app.get('/', (req, res) => {


    var ip = req.headers['x-forwarded-for'] || 
     req.connection.remoteAddress || 
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress;

     var ua = req.headers['user-agent'].split('(')[1].split(')')[0];

    res.json({
        "ipaddress": ip, 
        "language": req.acceptsLanguages()[0],
        "software": ua
    });
});

app.listen(app.get('port'), function () {
  console.log(`Request Header Parser Microservice listening on port ${app.get('port')}!`)
})